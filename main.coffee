@Dict = new Mongo.Collection "dict"
#Dict.initEasySearch ['en','ch'], {limit:12}

EasySearch.createSearchIndex 'dict',
  'field' : ['en']   # required, searchable field(s)
  'collection' : Dict      # required, Mongo Collection
  'limit' : 12             # not required, default is 10
  'use': 'mongo-db'
  'props' :
    'filteredCategories' : []
  'query' : (searchString)->
    #Default query that will be used for searching
    query = EasySearch.getSearcher(this.use).defaultQuery(this, searchString)

    #filter for categories if set
    if this.props.filteredCategories.length > 0
      query.category =
        $in : this.props.filteredCategories
    return query


if Meteor.isServer
  @category=''
  
  Meteor.startup ->
    @category = Dict.aggregate
      $group:
        _id: '$category'
        count: {$sum: 1}
    
  Meteor.methods
    'category':->
      return category
#  
#  menu = JSON.parse(Assets.getText 'menu.json')
#  
#  Meteor.startup ->
#    if !Dict.find().count()
#      menu.forEach (json_file) ->
#        JSON.parse(Assets.getText(json_file)).forEach (it) ->
#          Dict.insert it
#        console.log json_file+ 'inserted'
    

if Meteor.isClient
  
  
  Meteor.startup ->
    Meteor.call 'category', (err,result)->
      Session.set('unfiltered',result.sort (a,b)->
        b.count-a.count) unless err
    
    Session.set 'filtered', []

  Template.dict.events
    'mouseenter .corner': (e,t) ->
      e.stopPropagation()
      $(e.currentTarget).popup()

  Template.dict.helpers
    ensplit2:  ->
      query_str_low = $('#search').val().toLowerCase()
      if !!!query_str_low
        return false
      en = this.en
      start = en.toLowerCase().indexOf query_str_low
      query_str_nice = en.substring start, (start + query_str_low.length)
      pre_suf = en.split query_str_nice
      
      return {
        pre:pre_suf[0]
        q: query_str_nice
        suf:pre_suf[1]
      }

  Template.filters.helpers
    'filtered': ->
      Session.get 'filtered'
    'unfiltered': ->
      Session.get 'unfiltered'
        
        
  @transferObj = (lossArr, gainArr, str )->
    obj = Session.get(lossArr).find((o)->
      o._id ==str
    )
    Session.set lossArr , Session.get(lossArr).remove((o)->
      o._id ==str
    )
    Session.set gainArr, Session.get(gainArr).add(obj,0)
  
  @searchAgain = ()-> 
    instance = EasySearch.getComponentInstance(index: 'dict')

    # Change the currently filteredCategories like this
    EasySearch.changeProperty 'dict', 'filteredCategories', Session.get('filtered').map(
      (o)->o._id)

    # Trigger the search again, to reload the new products
    instance.triggerSearch()
    return
  
  Template.filters.events
    'click .filtered': (e)->
      
      e.stopPropagation()
      console.log 'filtered'
      transferObj 'filtered', 'unfiltered', $(e.currentTarget).attr('data')
      searchAgain()
      
    'click .unfiltered':(e)->
      e.stopPropagation()
      console.log $(e.currentTarget).attr('data')
      transferObj 'unfiltered', 'filtered', $(e.currentTarget).attr('data')
      searchAgain()


    
#    'mouseenter .card': (e,t) ->
#      e.stopPropagation()
#      $(e.currentTarget).transition('pulse')